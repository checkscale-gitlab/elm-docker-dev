if has('nvim')
  let s:term_buf = 0
  let s:term_win = 0

  function! Term_toggle(width)
      if win_gotoid(s:term_win)
          hide
      else
          botright vnew
          exec "vertical resize " . a:width
          try
              exec "buffer " . s:term_buf
          catch
              call termopen($SHELL, {"detach": 0})
              let s:term_buf = bufnr("")
          endtry
          startinsert!
          let s:term_win = win_getid()
      endif
  endfunction

  nnoremap ,t :call Term_toggle(80)<cr>
  tnoremap ,t <C-\><C-n>:call Term_toggle(80)<cr>
  tnoremap <Esc> <C-\><C-n>
endif
