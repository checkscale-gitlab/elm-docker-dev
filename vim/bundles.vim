" Base .vimrc file
" Installation Instructions
"	1. Place file in home directory as .vimrc
"	2. Run the following command in terminal
"		mkdir .vim .vim/bundle .vim/backup .vim/swap .vim/cache .vim/undo; curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"	3. Launch Vim and Run
"		:PlugInstall
" Or from shell vim +PlugInstall +qall
"	4. Restart Vim

call plug#begin('~/.vim/bundle')

"Languages
Plug 'git://github.com/othree/html5.vim.git', { 'for': [ 'html', 'eruby', 'haml' ] }
Plug 'git://github.com/hail2u/vim-css3-syntax.git', { 'for': [ 'css', 'scss' ] }
Plug 'git://github.com/JulesWang/css.vim.git', { 'for': [ 'css', 'scss' ] }
Plug 'git://github.com/pangloss/vim-javascript.git', { 'for': [ 'javascript', 'html' ] }
Plug 'git://github.com/Zaptic/elm-vim.git', { 'for': 'elm' }
Plug 'git://github.com/groteck/vim-elm-conceals.git', { 'for': 'elm' }
Plug 'git://github.com/elzr/vim-json.git', { 'for': 'json' }
Plug 'pangloss/vim-javascript'
Plug 'git://github.com/leafgarland/typescript-vim.git'
Plug 'peitalin/vim-jsx-typescript'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

"Formating text
Plug 'git://github.com/godlygeek/tabular.git'
Plug 'git://github.com/plasticboy/vim-markdown.git', { 'for': 'markdown' }
Plug 'git://github.com/tpope/vim-surround.git'

"Extract part of files with different syntax
Plug 'git://github.com/vim-scripts/ingo-library.git'
Plug 'git://github.com/vim-scripts/SyntaxRange.git'

"Commenting lines
Plug 'git://github.com/tomtom/tcomment_vim.git'

"Snipmate and all its dependencies
Plug 'git://github.com/SirVer/ultisnips.git'
Plug 'git://github.com/honza/vim-snippets.git'

"Syntax check
" Plug 'git://github.com/w0rp/ale.git'

"File explorer and grep
Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'junegunn/fzf.vim'

"Colorscheme
Plug 'git://github.com/altercation/vim-colors-solarized.git'

"Fancy icons to use into vim
Plug 'git://github.com/ryanoasis/vim-devicons.git'

"Autoclose
Plug 'git://github.com/Raimondi/delimitMate.git'

"Better status line
Plug 'git://github.com/itchyny/lightline.vim.git'

"Git support
Plug 'git://github.com/tpope/vim-fugitive.git'

"Undo branching
Plug 'git://github.com/sjl/gundo.vim.git', { 'on': 'GundoToggle' }

"Multiple cursors
Plug 'git://github.com/terryma/vim-multiple-cursors.git'

"Indentation lines
Plug 'git://github.com/nathanaelkane/vim-indent-guides.git'

"Current file output
Plug 'git://github.com/vim-scripts/Bexec.git'

" Autocomplete
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

call plug#end()

"
" Brief help
"
" :PlugInstall [name ...] - intall Plugins
" :PlugUpdate [name ...]  - update Plugins
" :PlugClean[!]           - Remove unused directories (bang version will clean
"                           without prompt)
"
" NOTE: comments after Plug command are not allowed..
